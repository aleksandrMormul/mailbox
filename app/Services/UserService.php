<?php

namespace App\Services;

use App\Http\Requests\User\GetRequests;
use App\Models\User;
use App\Repository\Eloquent\UserRepository;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;

/**
 * Class UserService
 * @package App\Services
 */
class UserService
{
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $userEmail
     * @return User
     */
    public function findUserByEmail($userEmail)
    {
        return $this->userRepository->findUserByEmail($userEmail);
    }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function userProfile()
    {
        return response()->json(auth()->user());
    }

    /**
     * Refresh a token.
     *
     * @return JsonResponse
     */
    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    protected function createNewToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth()->user(),
        ]);
    }

    /**
     * @param GetRequests $requests
     * @return mixed
     */
    public function collectUsers(GetRequests $requests)
    {
        return $this->userRepository->collectUser($requests->ids);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getUser($id)
    {
        return $this->userRepository->getUser($id);
    }

    /**
     * @param string $userEmail
     * @return bool
     * @throws Exception
     */
    public function checkUser($userEmail)
    {
        $isUser = $this->userRepository->checkUser($userEmail);
        if (!$isUser) {
            throw new ModelNotFoundException('This user is not present in system!', 404);
        }

        return true;
    }
}
