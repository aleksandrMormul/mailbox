<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\GetRequests;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends ApiController
{

    private $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param GetRequests $request
     * @return JsonResponse
     */
    public function getCollectUsers(GetRequests $request)
    {
        $users = $this->userService->collectUsers($request);
        return response()->json($users);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function getUser($id)
    {
        return response()->json($this->userService->getUser($id));
    }
}
