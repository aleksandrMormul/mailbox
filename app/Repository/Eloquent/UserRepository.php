<?php

namespace App\Repository\Eloquent;

use App\Models\User;

/**
 * Class UserRepository
 * @package App\Repository\Eloquent
 */
class UserRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * @param string $userEmail
     * @return mixed
     */
    public function findUserByEmail($userEmail)
    {
        return User::where('email', '=', $userEmail)->first();
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public function collectUser($ids)
    {
        return User::find($ids, ['id', 'name', 'email']);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getUser($id)
    {
        return $this->find($id)->get();
    }

    /**
     * @param string $userEmail
     * @return bool
     */
    public function checkUser($userEmail)
    {
        $data = User::where('email', $userEmail)->first();
        if ($data) {
            return true;
        }
        return false;
    }
}
