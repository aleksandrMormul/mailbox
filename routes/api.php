<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('user', function (Request $request) {
    return $request->user();
});
Route::group([
    'prefix' => 'auth'

], function ($router) {
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('register', 'AuthController@register');
    Route::post('refresh', 'AuthController@refresh');
});
Route::group([
    'middleware' => ['auth:api','jwt.verify'],
    'prefix' => 'auth'

], function ($router) {
    //Route::post('login', 'AuthController@login');
   // Route::post('register', 'AuthController@register');
    Route::get('user-profile', 'AuthController@getAuthenticatedUser');
    Route::get('closed', 'DataController@closed');
});

Route::group([
    'middleware' => ['jwt.verify'],
], function ($router) {
    Route::post('creates', 'MailController@createMail');
    Route::get('receivers', 'ReceiverController@getReceivers');
    Route::get('inboxs', 'MailController@getMails');
    Route::get('mails/{id}', 'MailController@getMail');
    Route::get('users/{id}', 'UserController@getUser');
    Route::get('collectusers', 'UserController@getCollectUsers');
    Route::put('reads', 'ReceiverController@setRead');
    //Route::get('users', 'UserController@getUsers');
});
