<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\EloquentRepositoryInterface;
use App\Repository\Eloquent\MailRepository;
use App\Repository\Eloquent\ReceiverRepository;
use App\Repository\Eloquent\UserRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, MailRepository::class);
        $this->app->bind(EloquentRepositoryInterface::class, UserRepository::class);
        $this->app->bind(EloquentRepositoryInterface::class, ReceiverRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
