<?php

namespace App\Http\Controllers;

use App\Http\Requests\Receiver\GetRequest;
use App\Http\Requests\Receiver\UpdateRequest;
use App\Services\ReceiverService;
use Illuminate\Http\JsonResponse;

/**
 * Class ReceiverController
 * @package App\Http\Controllers
 */
class ReceiverController extends ApiController
{

    private $receiverService;

    /**
     * ReceiverController constructor.
     * @param ReceiverService $receiverService
     */
    public function __construct(ReceiverService $receiverService)
    {
        $this->receiverService = $receiverService;
    }

    /**
     * @param GetRequest $request
     * @return JsonResponse|mixed
     */
    public function getReceivers(GetRequest $request)
    {
        return $this->receiverService->findReceiversByParam($request);
    }

    /**
     * @param UpdateRequest $request
     * @return JsonResponse|void
     */
    public function setRead(UpdateRequest $request)
    {
        return $this->receiverService->setRead($request);
    }
}
