<?php

namespace App\Http\Requests\Mail;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property string $to
 * @property string $msg
 * @property string $subject
 */
class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'to' => 'required|max:50',
            'msg' => 'required',
            'subject' => 'required',
        ];
    }
}
