<?php

namespace App\Http\Controllers;

use App\Http\Requests\Mail\CreateRequest;
use App\Http\Requests\Mail\GetRequests;
use App\Services\MailService;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;

/**
 * Class MailController
 * @package App\Http\Controllers
 */
class MailController extends ApiController
{
    private $mailService;

    /**
     * MailController constructor.
     * @param MailService $mailService
     */
    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * @param CreateRequest $request
     * @return Model
     * @throws Exception
     */
    public function createMail(CreateRequest $request)
    {
        return $this->mailService->createMail($request);
    }

    /**
     * @param GetRequests $request
     * @return JsonResponse|mixed
     */
    public function getMails(GetRequests $request)
    {
        return $this->mailService->getMails($request);
    }

    /**
     * @param int $id
     * @return Model|JsonResponse
     */
    public function getMail($id)
    {
        return $this->mailService->getMail($id);
    }
}
