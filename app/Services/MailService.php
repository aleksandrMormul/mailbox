<?php

namespace App\Services;

use App\Http\Requests\Mail\CreateRequest;
use App\Http\Requests\Mail\GetRequests;
use App\Repository\Eloquent\MailRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MailService
 * @package App\Services
 */
class MailService
{
    private $mailRepository;

    /**
     * MailService constructor.
     * @param MailRepository $mailRepository
     */
    public function __construct(MailRepository $mailRepository)
    {
        $this->mailRepository = $mailRepository;
    }

    /**
     * @param CreateRequest $request
     * @return Model
     * @throws Exception
     */
    public function createMail(CreateRequest $request)
    {
        /**
         * @var UserService $userService
         * @var ReceiverService $receiverService
         */
        $userService = resolve(UserService::class);
        $receiverService = resolve(ReceiverService::class);

        $ownerId = auth()->id();
        $userService->checkUser($request->to);
        $recipientId = $userService->findUserByEmail($request->to)->id;
        $mailId = $this->mailRepository->createMail([
            'body' => $request->msg,
            'subject' => $request->subject,
            ])->id;
        return $receiverService->create($mailId, $ownerId, $recipientId);
    }

    /**
     * @param GetRequests $request
     * @return mixed
     */
    public function getMails(GetRequests $request)
    {
        return $this->mailRepository->getMails($request->ids);
    }

    /**
     * @param $id
     * @return Model
     */
    public function getMail($id)
    {
        return $this->mailRepository->getMail($id);
    }
}
