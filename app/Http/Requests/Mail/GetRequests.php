<?php

namespace App\Http\Requests\Mail;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GetRequests
 * @property array $ids
 * @package App\Http\Requests\Mail
 */
class GetRequests extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ids' => 'required|array|nullable|min:0',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'ids' => json_decode((string)$this->ids, true) ?: null,
        ]);
    }
}
