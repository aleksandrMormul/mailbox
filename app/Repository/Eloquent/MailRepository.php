<?php

namespace App\Repository\Eloquent;

use App\Models\Mail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class MailRepository
 * @package App\Repository\Eloquent
 */
class MailRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param Mail $model
     */
    public function __construct(Mail $model)
    {
        parent::__construct($model);
    }

    /**
     * @param $data
     * @return Model
     */
    public function createMail($data)
    {
        return $this->create($data);
    }

    /**
     * @param int $id
     * @return Model
     */
    public function getMail($id)
    {
        return $this->find($id);
    }

    /**
     * @param array $ids
     * @return Collection
     */
    public function getMails($ids)
    {
        return Mail::whereIn('id', $ids)->get();
    }
}
