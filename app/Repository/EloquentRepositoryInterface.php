<?php

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

/**
 * Interface EloquentRepositoryInterface
 * @package App\Repositories
 */
interface EloquentRepositoryInterface
{
    /**
     * @param array $attributes
     * @return Model
     */
    public function create(array $attributes);

    /**
     * @param $id
     * @return Model
     */
    public function find($id);

    /**
     * @param $id
     * @return Model
     */
    public function delete($id);

    /**
     * @param $id
     * @param array $data
     * @return Model
     */
    public function update($id, array $data);
}
