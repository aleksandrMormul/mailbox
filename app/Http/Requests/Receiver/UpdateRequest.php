<?php

namespace App\Http\Requests\Receiver;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class GetRequests
 * @property int $mailId
 * @package App\Http\Requests\User
 */
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mailId' => 'required|int',
        ];
    }
}
