<?php

namespace App\Repository\Eloquent;

use App\Models\Receiver;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReceiverRepository
 * @package App\Repository\Eloquent
 */
class ReceiverRepository extends BaseRepository
{
    /**
     * UserRepository constructor.
     *
     * @param Receiver $model
     */
    public function __construct(Receiver $model)
    {
        parent::__construct($model);
    }

    /**
     * @param $data
     * @return Model
     */
    public function createReceiver($data)
    {
        return $this->create($data);
    }

    /**
     * @param int $recipientId
     * @param string $mailType
     * @return mixed
     */
    public function findReceiverByParam($recipientId, $mailType)
    {
        return Receiver::where('recipient', '=', $recipientId)->type($mailType)->get();
    }

    /**
     * @param int $mailId
     * @return bool|int|void
     */
    public function setRead($mailId)
    {
        $isRead = Receiver::where('mail_id', '=', $mailId)->pluck('is_read');
        if ($isRead[0] !== null) {
            return;
        }
        return Receiver::where('mail_id', '=', $mailId)->update(['is_read' => \Illuminate\Support\Carbon::now()]);
    }
}
