<?php

namespace App\Services;

use App\Http\Requests\Receiver\GetRequest;
use App\Http\Requests\Receiver\UpdateRequest;
use App\Repository\Eloquent\ReceiverRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ReceiverService
 * @package App\Services
 */
class ReceiverService
{
    private $receiverRepository;

    /**
     * ReceiverService constructor.
     * @param ReceiverRepository $receiverRepository
     */
    public function __construct(ReceiverRepository $receiverRepository)
    {
        $this->receiverRepository = $receiverRepository;
    }

    /**
     * @param int $mailId
     * @param int $recipientId
     * @param string $owner
     * @return Model
     */
    public function create($mailId, $owner, $recipientId)
    {
        return $this->receiverRepository->createReceiver([
            'mail_id' => $mailId,
            'owner' => $owner,
            'recipient' => $recipientId
        ]);
    }

    /**
     * @param GetRequest $request
     * @return mixed
     */
    public function findReceiversByParam(GetRequest $request)
    {
        $userId = auth()->id();
        $mailType = $request->type;
        return $this->receiverRepository->findReceiverByParam($userId, $mailType);
    }

    /**
     * @param UpdateRequest $request
     * @return void
     */
    public function setRead(UpdateRequest $request)
    {
        return $this->receiverRepository->setRead($request->mailId);
    }
}
