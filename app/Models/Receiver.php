<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Date;

/**
 * Class Receiver
 *
 * @property int $id
 * @property int $mail_id
 * @property int $owner
 * @property int $recipient
 * @property Date $mail_type
 * @property string $starred
 * @package App\Models
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $is_read
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver newQuery()
 * @method static \Illuminate\Database\Query\Builder|Receiver onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver query()
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver type($type)
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver whereIsRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver whereMailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver whereMailType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver whereOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver whereRecipient($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver whereStarred($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Receiver whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Receiver withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Receiver withoutTrashed()
 * @mixin \Eloquent
 */
class Receiver extends Model
{
    use SoftDeletes;

    protected $fillable = ['mail_id', 'owner', 'recipient'];

    /**
     * @return HasMany
     */
    public function mails()
    {
        return $this->hasMany('Mail');
    }

    /**
     * @param $query
     * @param $type
     * @return mixed
     */
    public function scopeType($query, $type)
    {
        return $query->where('mail_type', $type);
    }
}
